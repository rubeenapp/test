package com.kla.api.user;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kla.api.authorization.LoginForm;
import com.kla.api.authorization.SignUpForm;
import com.kla.api.exception.ResourceNotFoundException;
import com.kla.api.role.Role;
import com.kla.api.role.RoleName;
import com.kla.api.role.RoleRepository;
import com.kla.api.security.JwtProvider;
import com.kla.api.security.JwtResponse;
import com.kla.api.user.User;
import com.kla.api.user.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/kla/api")
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/user/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						loginRequest.getUsername(),
						loginRequest.getPassword()
						)
				);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateJwtToken(authentication);
		return ResponseEntity.ok(new JwtResponse(jwt));
	}

	@PostMapping("/user/signup")
	public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		if(userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<String>("Fail -> Username is already taken!",
					HttpStatus.BAD_REQUEST);
		}

		if(userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<String>("Fail -> Email is already in use!",
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
				signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();
		strRoles.forEach(role -> {
			switch(role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);
				break;
			case "minister":
				Role ministerRole = roleRepository.findByName(RoleName.ROLE_MINISTER)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(ministerRole);
				break;
			case "lawDept":
				Role lawDeptRole = roleRepository.findByName(RoleName.ROLE_LAWDEPT)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(lawDeptRole);
				break;
			case "officeSection":
				Role officeSectionRole = roleRepository.findByName(RoleName.ROLE_OFFICESECTION)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(officeSectionRole);
				break;
			case "legislationDept":
				Role legislationDeptRole = roleRepository.findByName(RoleName.ROLE_LEGISLATIONDEPT)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(legislationDeptRole);
				break;
			case "bac":
				Role bacRole = roleRepository.findByName(RoleName.ROLE_BAC)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(bacRole);
				break;
			case "subjectCommittee":
				Role subjectCommitteeRole = roleRepository.findByName(RoleName.ROLE_SUBJECTCOMMITTEE)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(subjectCommitteeRole);
				break;
			case "tableSection":
				Role tableSectionRole = roleRepository.findByName(RoleName.ROLE_TABLESECTION)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(tableSectionRole);
				break;
			case "mla":
				Role mlaRole = roleRepository.findByName(RoleName.ROLE_MLA)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(mlaRole);
				break;
			case "speaker":
				Role speakerRole = roleRepository.findByName(RoleName.ROLE_SPEAKER)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(speakerRole);
				break;
			case "governor":
				Role governorRole = roleRepository.findByName(RoleName.ROLE_GOVERNOR)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(governorRole);
				break;
			case "niyamasabhaPress":
				Role niyamasabhaPressRole = roleRepository.findByName(RoleName.ROLE_NIYAMASABHAPRESS)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(niyamasabhaPressRole);
				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_PUBLICUSER)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);        			
			}
		});

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok().body("User registered successfully!");
	}


	@GetMapping("/users") 
	//	@PreAuthorize("hasRole('user') or hasRole('ADMIN')")
	public ResponseEntity<List<User>> getUsers() { 
		List<User> users= userRepository.findAll();

		if (!users.isEmpty()) {
			System.out.println("in if");
			return ResponseEntity.ok().body(users);
		}else{
			System.out.println("in else");
			throw new ResourceNotFoundException("No Users Yet");
		}
	}

	/*
	 * @PutMapping("/users/{userId}") public User updatePassword(@PathVariable Long
	 * userId,@RequestBody User userReq) { return
	 * userRepository.findById(userId).map(user -> { if(userReq.getPassword()!=null)
	 * { encoder.encode(userReq.getPassword()); } return userRepository.save(user);
	 * }).orElseThrow(() -> new ResourceNotFoundException("User Id " + userId +
	 * " not found")); }
	 */

	@PutMapping("/user/{userId}") 
	public User updateUserDetails(@PathVariable Long userId,@RequestBody User userReq) {
		return userRepository.findById(userId).map(user -> {
			if(userReq.getName()!=null) {
				user.setName(userReq.getName());
			}
			if(userReq.getEmail()!=null) {
				user.setEmail(userReq.getEmail());
			}
			if(userReq.getRoles()!=null) {
				user.setRoles(userReq.getRoles());
			}
			return userRepository.save(user);
		}).orElseThrow(() -> new ResourceNotFoundException("User Id " + userId + " not found"));
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<?> getUserById(@PathVariable Long userId) {
		return userRepository.findById(userId)
				.map(user -> {
					return ResponseEntity.ok(user);
				}).orElseThrow(() -> new ResourceNotFoundException("user not found with id " + userId));
	}	

	@DeleteMapping("/user/{userId}")
	public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
		return userRepository.findById(userId)
				.map(user -> {
					userRepository.delete(user);
					return ResponseEntity.ok().build();
				}).orElseThrow(() -> new ResourceNotFoundException("User not found with id " + userId));
	}
}