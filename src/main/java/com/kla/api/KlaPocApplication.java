package com.kla.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KlaPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlaPocApplication.class, args);
	}

}
