package com.kla.api.part2bulletin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BulletinLinesRepository extends JpaRepository<BulletinLines, Long> {
}