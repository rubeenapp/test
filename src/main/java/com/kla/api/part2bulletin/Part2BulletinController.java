package com.kla.api.part2bulletin;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.kla.api.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/kla/api")
public class Part2BulletinController {

	@Autowired
	private Part2BulletinRepository Part2BulletinRepository;
	
	@PostMapping("/Part2Bulletin")
    public ResponseEntity<String> createPart2Bulletin(@RequestBody Part2Bulletin Part2Bulletin) {
        Part2BulletinRepository.save(Part2Bulletin);
        return ResponseEntity.ok().body("created successfully!");
    }
	
	@GetMapping("/Part2Bulletins") 
	public List<Part2Bulletin> getPart2Bulletins() { 
		return Part2BulletinRepository.findAll(); 
	}

	@PutMapping("/Part2Bulletin/{Part2BulletinId}") 
	public ResponseEntity<String> updatePart2Bulletin(@Valid @RequestParam Part2Bulletin Part2Bulletin) {
	    Part2BulletinRepository.save(Part2Bulletin);
	    return ResponseEntity.ok().body("updated successfully!");
	}
	
	@GetMapping("/Part2Bulletin/{Part2BulletinId}")
    public ResponseEntity<?> getPart2Bulletin(@PathVariable Long Part2BulletinId) {
        return Part2BulletinRepository.findById(Part2BulletinId)
                .map(Part2Bulletin -> {
                	return ResponseEntity.ok(Part2Bulletin);
                }).orElseThrow(() -> new ResourceNotFoundException("Part2Bulletin not found with id " + Part2BulletinId));
    }	
	
	@DeleteMapping("/Part2Bulletin/{Part2BulletinId}")
    public ResponseEntity<?> deletePart2Bulletin(@PathVariable Long Part2BulletinId) {
        return Part2BulletinRepository.findById(Part2BulletinId)
                .map(Part2Bulletin -> {
                	Part2BulletinRepository.delete(Part2Bulletin);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Part2Bulletin not found with id " + Part2BulletinId));
    }	

}
