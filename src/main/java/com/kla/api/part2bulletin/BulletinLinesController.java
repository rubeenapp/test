package com.kla.api.part2bulletin;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kla.api.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/kla/api")
public class BulletinLinesController {

	@Autowired
	private BulletinLinesRepository BulletinLinesRepository;
	
	@PostMapping("/BulletinLine")
    public ResponseEntity<String> createBulletinLines(@RequestBody BulletinLines BulletinLines) {
        BulletinLinesRepository.save(BulletinLines);
        return ResponseEntity.ok().body("created successfully!");
    }
	
	@GetMapping("/BulletinLines") 
	public List<BulletinLines> getBulletinLiness() { 
		return BulletinLinesRepository.findAll(); 
	}
	
	@PutMapping("/BulletinLine/{BulletinLinesId}") 
	public ResponseEntity<String> updateBulletinLines(@Valid @RequestParam BulletinLines BulletinLines) {
	    BulletinLinesRepository.save(BulletinLines);
	    return ResponseEntity.ok().body("updated successfully!");
	}
	
	@GetMapping("/BulletinLine/{BulletinLinesId}")
    public ResponseEntity<?> getBulletinLines(@PathVariable Long BulletinLinesId) {
        return BulletinLinesRepository.findById(BulletinLinesId)
                .map(BulletinLines -> {
                	return ResponseEntity.ok(BulletinLines);
                }).orElseThrow(() -> new ResourceNotFoundException("BulletinLines not found with id " + BulletinLinesId));
    }	
	
	@DeleteMapping("/BulletinLine/{BulletinLinesId}")
    public ResponseEntity<?> deleteBulletinLines(@PathVariable Long BulletinLinesId) {
        return BulletinLinesRepository.findById(BulletinLinesId)
                .map(BulletinLines -> {
                	BulletinLinesRepository.delete(BulletinLines);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("BulletinLines not found with id " + BulletinLinesId));
    }	

}
