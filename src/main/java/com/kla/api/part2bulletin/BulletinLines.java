package com.kla.api.part2bulletin;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.kla.api.bill.Bill;
import com.kla.api.model.AuditModel;

@Entity
@Table(name = "BulletinLines")
public class BulletinLines extends AuditModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne
	private Part2Bulletin part2Bulletin;
	
	@OneToOne
	@JoinColumn(name = "bill_id")
	private Bill blbill;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Part2Bulletin getPart2Bulletin() {
		return part2Bulletin;
	}

	public void setPart2Bulletin(Part2Bulletin part2Bulletin) {
		this.part2Bulletin = part2Bulletin;
	}

	public Bill getBlbill() {
		return blbill;
	}

	public void setBlbill(Bill blbill) {
		this.blbill = blbill;
	}
	
	
}