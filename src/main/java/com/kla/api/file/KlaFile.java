package com.kla.api.file;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.*;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GenericGenerator;

import com.kla.api.bill.Bill;
import com.kla.api.model.AuditModel;

@Entity
@Table(name = "klaFile")
public class KlaFile extends AuditModel {

	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private UUID id;

	@Column(columnDefinition = "serial")
	@org.hibernate.annotations.Generated(GenerationTime.INSERT)
	private long fileNumber;
	
	@Column
	private String status;

	@Column
	private long NumberOfFiles;

	@OneToMany(mappedBy = "kFile") 
	private List<Bill> bills1 = new ArrayList<Bill>();
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public List<Bill> getBills() {
		return bills1;
	}

	public void setBills(List<Bill> bills1) {
		this.bills1 = bills1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getNumberOfFiles() {
		return NumberOfFiles;
	}

	public void setNumberOfFiles(long numberOfFiles) {
		NumberOfFiles = numberOfFiles;
	}

	public List<Bill> getBills1() {
		return bills1;
	}

	public void setBills1(List<Bill> bills1) {
		this.bills1 = bills1;
	}
	
	public long getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(long fileNumber) {
		this.fileNumber = fileNumber;
	}

}
