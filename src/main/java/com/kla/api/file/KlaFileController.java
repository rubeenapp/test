package com.kla.api.file;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kla.api.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/kla/api")
public class KlaFileController {
	
	@Autowired
	private KlaFileRepository klaFileRepository;

	@PostMapping("/klafile")
	public ResponseEntity<String> createklaFile(@RequestBody KlaFile klaFile) {
		klaFileRepository.save(klaFile);
		return ResponseEntity.ok().body("File created successfully!");
	}

	@GetMapping("/klafiles") 
	public List<KlaFile> getklaFiles() { 
		return  klaFileRepository.findAll(); 
	}
	
	@PutMapping("/klafile/{klaFileId}") 
	public ResponseEntity<String> updateklaFile(@Valid @RequestParam KlaFile klaFile) {
	    klaFileRepository.save(klaFile);
	    return ResponseEntity.ok().body("File updated successfully!");
	}
	
	@GetMapping("/klafile/{klaFileId}")
    public ResponseEntity<?> getklaFile(@PathVariable Long klaFileId) {
        return klaFileRepository.findById(klaFileId)
                .map(klaFile -> {
                	return ResponseEntity.ok(klaFile);
                }).orElseThrow(() -> new ResourceNotFoundException("kla File not found with id " + klaFileId));
    }	

	@DeleteMapping("/klafile/{klaFileId}")
	public ResponseEntity<?> deleteklaFile(@PathVariable Long klaFileId) {
		return klaFileRepository.findById(klaFileId)
				.map(klaFile -> {
					klaFileRepository.delete(klaFile);
					return ResponseEntity.ok().body("File deleted successfully!");
				}).orElseThrow(() -> new ResourceNotFoundException("kla File not found with id " + klaFileId));
	}
}
