package com.kla.api.bill;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kla.api.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/kla/api")
public class BillController {
	
	@Autowired
	private BillRepository billRepository;
	
	@PostMapping("/bill")
    public ResponseEntity<String> createBill(@RequestBody Bill bill) {
        billRepository.save(bill);
        return ResponseEntity.ok().body("Bill created successfully!");
    }
	
	@GetMapping("/bills") 
	public List<Bill> getBills() { 
		return billRepository.findAll(); 
	}
	
	@PutMapping("/bill/{billId}") 
	public ResponseEntity<String> updateBill(@Valid @RequestBody Bill bill) {
	    billRepository.save(bill);
	    return ResponseEntity.ok().body("Bill updated successfully!");
	}
	

	@GetMapping("/bill/{billId}")
    public ResponseEntity<?> getBill(@PathVariable Long billId) {
        return billRepository.findById(billId)
                .map(bill -> {
                	return ResponseEntity.ok(bill);
                }).orElseThrow(() -> new ResourceNotFoundException("bill not found with id " + billId));
    }	
	
	@DeleteMapping("/bill/{billId}")
    public ResponseEntity<?> deleteBill(@PathVariable Long billId) {
        return billRepository.findById(billId)
                .map(bill -> {
                	billRepository.delete(bill);
                	return ResponseEntity.ok().body("bill deleted successfully!");
                }).orElseThrow(() -> new ResourceNotFoundException("bill not found with id " + billId));
    }	
}
