package com.kla.api.amendment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AmendmentRepository extends JpaRepository<Amendment, Long>  {
}