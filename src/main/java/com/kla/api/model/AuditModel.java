package com.kla.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"createdAt", "updatedAt"},
        allowGetters = true
)
public abstract class AuditModel implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private Date createdAt=new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    @LastModifiedDate
    private Date updatedAt = new Date();

    public Date getCreatedAt() {
    	System.out.println("in getCreatedAt()");
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
    	System.out.println("in setCreatedAt()");
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
    	System.out.println("in getUpdatedAt()");
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
    	System.out.println("in setUpdatedAt()");
        this.updatedAt = updatedAt;
    }
}